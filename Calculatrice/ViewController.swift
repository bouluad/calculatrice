//
//  ViewController.swift
//  Calculatrice
//
//  Created by Mohammed Bouluad on 28/12/2016.
//  Copyright © 2016 Mohammed. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    
    var resultat = 0.0
    var operation = ""
    var n1 = 0.0
    var n2 = 0.0
    
    var premier = true
    
    @IBOutlet var ViewLabel: UILabel!
 
    @IBAction func Cbtn(_ sender: Any) {
        
      ViewLabel.text="0"
        operation = ""
    }
    
    
    @IBAction func Parenthesebtn(_ sender: Any) {
        // Not yet
    }
    @IBAction func modulobtn(_ sender: Any) {
        if (operation == "") {
            
            operation = "%"
            ViewLabel.text = ViewLabel.text! + "%"
        }
    }
    
    @IBAction func divbtn(_ sender: Any) {
        
        if (operation == "") {
            
            operation = "/"
            ViewLabel.text = ViewLabel.text! + "/"
        }
    }
    
    @IBAction func multibtn(_ sender: Any) {
        
        if (operation == "") {
            
            operation = "*"
            ViewLabel.text = ViewLabel.text! + "*"
        }
    }
    
    @IBAction func moinsbtn(_ sender: Any) {
        
        if (operation == "") {
            
            operation = "-"
            ViewLabel.text = ViewLabel.text! + "-"
        }
    }
    
    @IBAction func plusbtn(_ sender: Any) {
        
        if (operation == "") {
            
            operation = "+"
            ViewLabel.text = ViewLabel.text! + "+"
        }
        
    }
    
    @IBAction func virgbtn(_ sender: Any) {
        // Not yet
    }
    
    @IBAction func plusmoinsbtn(_ sender: Any) {
        // Not yet
    }
    
    @IBAction func egalebtn(_ sender: Any) {
        
        if (operation != "") {
            
              var arr = ViewLabel.text?.components(separatedBy: operation)
              n1 = Double(arr![0])!
              n2 = Double(arr![1])!
        
            switch operation {
            case "+":
                     resultat = addition(numero1: n1, numero2: n2)
            case "-":
                     resultat = soustraction(numero1: n1, numero2: n2)
            case "*":
                     resultat = multiplication(numero1: n1, numero2: n2)
            case "/":
                     resultat = division(numero1: n1, numero2: n2)
            case "%":
                     resultat = modulo(numero1: n1, numero2: n2)
            default:
                print("Other Operation")
            }
            
            ViewLabel.text = String(resultat)
            operation = ""
          }
    
    }
    
    @IBAction func zero(_ sender: Any) {
        
        if (ViewLabel.text=="0"){
            
            ViewLabel.text = "0"
        }
        else {
            
            ViewLabel.text = ViewLabel.text! + "0"
            
        }
    }
    
    @IBAction func unbtn(_ sender: Any) {
        
        if (ViewLabel.text=="0"){
            
            ViewLabel.text = "1"
        }
        else {
            
            ViewLabel.text = ViewLabel.text! + "1"
            
            }
    }
    
    @IBAction func deuxbtn(_ sender: Any) {
        if (ViewLabel.text=="0"){
            
            ViewLabel.text = "2"
        }
        else {
            
            ViewLabel.text = ViewLabel.text! + "2"
            
        }
    }
    
    @IBAction func troisbtn(_ sender: Any) {
        if (ViewLabel.text=="0"){
            
            ViewLabel.text = "3"
        }
        else {
            
            ViewLabel.text = ViewLabel.text! + "3"
            
        }
    }
    
    @IBAction func quatrebtn(_ sender: Any) {
        
        if (ViewLabel.text=="0"){
            
            ViewLabel.text = "4"
        }
        else {
            
            ViewLabel.text = ViewLabel.text! + "4"
            
        }
    }
    @IBAction func cinqbtn(_ sender: Any) {
        
        if (ViewLabel.text=="0"){
            
            ViewLabel.text = "5"
        }
        else {
            
            ViewLabel.text = ViewLabel.text! + "5"
            
        }
    }
    
    @IBAction func sixbtn(_ sender: Any) {
        
        if (ViewLabel.text=="0"){
            
            ViewLabel.text = "6"
        }
        else {
            
            ViewLabel.text = ViewLabel.text! + "6"
            
        }
    }
    
    @IBAction func septbtn(_ sender: Any) {
        
        if (ViewLabel.text=="0"){
            
            ViewLabel.text = "7"
        }
        else {
            
            ViewLabel.text = ViewLabel.text! + "7"
            
        }
    }
    @IBAction func huitbtn(_ sender: Any) {
        
        if (ViewLabel.text=="0"){
            
            ViewLabel.text = "8"
        }
        else {
            
            ViewLabel.text = ViewLabel.text! + "8"
            
        }
    }
    @IBAction func neufbtn(_ sender: Any) {
        
        if (ViewLabel.text=="0"){
            
            ViewLabel.text = "9"
        }
        else {
            
            ViewLabel.text = ViewLabel.text! + "9"
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     func addition(numero1: Double, numero2 : Double) -> Double {

        return (numero1 + numero2)
    }
     func soustraction(numero1: Double, numero2 : Double) -> Double {
        
        return numero1 - numero2
    }
     func multiplication(numero1: Double, numero2 : Double) -> Double {
        
        return numero1 * numero2
    }
     func division(numero1: Double, numero2 : Double) -> Double {
        
        var res = 0.0
        
        if numero2 != 0{
           
            res = numero1 / numero2 
        }
        
        return res
    }
     func modulo(numero1: Double, numero2 : Double) -> Double {
        
       return Double(numero1.truncatingRemainder(dividingBy: Double(numero2)))
        
       
    }
    
}

