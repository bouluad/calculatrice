//
//  CalculatriceTests.swift
//  CalculatriceTests
//
//  Created by Mohammed Bouluad on 28/12/2016.
//  Copyright © 2016 Mohammed. All rights reserved.
//

import XCTest
@testable import Calculatrice

class CalculatriceTests: XCTestCase {
    
    var viewctrl : ViewController!
    var n1 : Double!
    var n2 : Double!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let  storyboard = UIStoryboard(name: "Main", bundle : Bundle.main)
        viewctrl = storyboard.instantiateInitialViewController() as! ViewController
        
        n1 = 13.39
        n2 = -17.96
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        n1 = 0.0
        n2 = 0.0
    }
    
    func testAddition() {
      
        XCTAssertTrue(viewctrl.addition(numero1: n1, numero2: n2) == -4.57)

    }
    
    func testDivision() {
        
        XCTAssertTrue(viewctrl.division(numero1: 77, numero2: -22) == -3.5)
        
    }
    
    func testModulo() {
        
        XCTAssertTrue(viewctrl.modulo(numero1: 33, numero2: 11) == 0)
        
    }
    
    func testSoustraction() {
        
        XCTAssertTrue(viewctrl.soustraction(numero1: n1, numero2: n2) == 31.35)
        
    }
    
    func testMultiplication() {
        
        XCTAssertTrue(viewctrl.multiplication(numero1: 2369, numero2: 11) == 26059)
        
    }
    
    
    func testPerformanceExample() {
        self.measure {
            // Measure the time of this operation
           self.viewctrl.addition(numero1: self.n1, numero2: self.n2)
            
        }
    }
    
}
